import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
  apiKey: "AIzaSyCHhVPJXZyd8kPzuluZgno_cGvTSQWBSds",
  authDomain: "floreria-2a337.firebaseapp.com",
  databaseURL: "https://floreria-2a337-default-rtdb.firebaseio.com",
  projectId: "floreria-2a337",
  storageBucket: "floreria-2a337.appspot.com",
  messagingSenderId: "724316109487",
  appId: "1:724316109487:web:8d15d7a99196e7c321325a"
};
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    ListarProductos();
    mostrarProductos();
  });

  var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');

var idArreglo = "";
var nombreArreglo = "";
var precio= "";
var url= "";

function leerInputs() {
    idArreglo = document.getElementById('txtNumFlor').value;
    nombreArreglo = document.getElementById('txtNombre').value;
    precio = document.getElementById('txtPrecio').value;
    url = document.getElementById('url').value;
  }

  function insertarDatos() {
    leerInputs();
    set(refS(db, 'Productos/' + idArreglo), {
      nombre: nombreArreglo,
      Precio: precio,
      url: url
    }).then(() => {
      alert("Se insertó con éxito");
    }).catch((error) => {
      alert("Ocurrió un error: " + error);
    });
    limpiarInputs();
    ListarProductos();
  }
   
  function buscarDatos() {
    idArreglo = document.getElementById('txtNumFlor').value;
    const dbRef = refS(db, 'Productos/' + idArreglo);
    get(dbRef).then((snapshot) => {
      if (snapshot.exists()) {
        const data = snapshot.val();
        nombreArreglo = data.nombre;
        precio = data.Precio;
        url = data.url;
        escribirInputs();
      } else {
        alert("El Arreglo " + idArreglo + " no existe");
      }
    }).catch((error) => {
      alert("Ocurrió un error: " + error);
    });
  }

  function escribirInputs() {
    document.getElementById('txtNombre').value = nombreArreglo;
    document.getElementById('txtPrecio').value = precio;
    document.getElementById('url').value = url;
  }

  function ListarProductos() {
    const dbRef = refS(db, 'Productos');
    const tabla = document.getElementById('tablaFlores');
    const tbody = tabla.querySelector('tbody');
   
    tbody.innerHTML = '';
   
    onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const key = childSnapshot.key;
        const data = childSnapshot.val();
   
        var fila = document.createElement('tr');
   
        var celdaNumArreglo = document.createElement('td');
        celdaNumArreglo.textContent = key;
        fila.appendChild(celdaArreglo);
   
        var celdaNombreArreglo = document.createElement('td');
        celdaNombreArreglo.textContent = data.nombre;
        fila.appendChild(celdaNombreArreglo);
   
        var celdaPrecio = document.createElement('td');
        celdaPrecio.textContent = data.Precio;
        fila.appendChild(celdaPrecio);
   
        tbody.appendChild(fila);
      });
    }, { onlyOnce: true });
  }

  function actualizarDatos() {
    leerInputs();
    if (idArreglo === "" || nombreArreglo === "" || precio === "") {
      alert("Falta capturar información");
    } else {
      update(refS(db, 'Productos/' + idArreglo), {
        nombre: nombreArreglo,
        Precio: precio,
        url: url
      }).then(() => {
        alert("Se actualizó con éxito");
        limpiarInputs();
      }).catch((error) => {
        alert("Ocurrió un error: " + error);
      });
    }
    ListarProductos();
  }

  function eliminarProducto() {
    idArreglo = document.getElementById('txtNumFlor').value;
    remove(refS(db, 'Productos/' + idArreglo)).then(() => {
      alert("Producto eliminado con éxito");
      limpiarInputs();
      ListarProductos();
    }).catch((error) => {
      alert("Ocurrió un error al eliminar el producto: " + error);
    });
  }
   
  function limpiarInputs() {
    document.getElementById('txtNumFlor').value = '';
    document.getElementById('txtNombre').value = '';
    document.getElementById('txtPrecio').value = '';
    document.getElementById('url').value = '';
  }
  
  
    
   
  btnBorrar.addEventListener('click', eliminarProducto);
  btnAgregar.addEventListener('click', insertarDatos);
  btnActualizar.addEventListener('click', actualizarDatos);
  btnBuscar.addEventListener('click', buscarDatos);