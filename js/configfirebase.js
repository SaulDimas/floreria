
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.22.0/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyCHhVPJXZyd8kPzuluZgno_cGvTSQWBSds",
    authDomain: "floreria-2a337.firebaseapp.com",
    databaseURL: "https://floreria-2a337-default-rtdb.firebaseio.com",
    projectId: "floreria-2a337",
    storageBucket: "floreria-2a337.appspot.com",
    messagingSenderId: "724316109487",
    appId: "1:724316109487:web:8d15d7a99196e7c321325a"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
