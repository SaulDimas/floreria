import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';

const firebaseConfig = {
    apiKey: "AIzaSyCHhVPJXZyd8kPzuluZgno_cGvTSQWBSds",
    authDomain: "floreria-2a337.firebaseapp.com",
    databaseURL: "https://floreria-2a337-default-rtdb.firebaseio.com",
    projectId: "floreria-2a337",
    storageBucket: "floreria-2a337.appspot.com",
    messagingSenderId: "724316109487",
    appId: "1:724316109487:web:8d15d7a99196e7c321325a"
};


const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);


const formulario = document.querySelector("form");
const emailInput = document.getElementById("txtCorreo");
const passwordInput = document.getElementById("txtContraseña");
const signInButton = document.querySelector("button");
const errorMensaje = document.getElementById("errorMensaje");

formulario.addEventListener("submit", (event) => {
  event.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      window.location.href = "/html/Administrador.html";
    })
    .catch((error) => {
      console.log("Error al iniciar sesión:", error);
      errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});
