import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {   
    apiKey: "AIzaSyCHhVPJXZyd8kPzuluZgno_cGvTSQWBSds",
    authDomain: "floreria-2a337.firebaseapp.com",
    databaseURL: "https://floreria-2a337-default-rtdb.firebaseio.com",
    projectId: "floreria-2a337",
    storageBucket: "floreria-2a337.appspot.com",
    messagingSenderId: "724316109487",
    appId: "1:724316109487:web:8d15d7a99196e7c321325a"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    mostrarProductos();
  });
 
 
  function mostrarProductos() {
    const dbRef = refS(db, 'Productos');
    const contenedorProductos = document.getElementById('productos-container');
 
    contenedorProductos.innerHTML = '';
 
    onValue(dbRef, (snapshot) => {
      contenedorProductos.innerHTML = '';
 
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
 
        // Create column div for the product
        const divColumn = document.createElement('div');
        divColumn.classList.add('column');
 
        // Create product div
        const divProducto = document.createElement('div');
        divProducto.classList.add('producto');
 
        // Create heading element for the product name
        const h3Producto = document.createElement('h3');
        h3Producto.textContent = data.nombre;
        divProducto.appendChild(h3Producto);
 
        // Create image element
        const imgProducto = document.createElement('img');
        imgProducto.src = data.url;
        imgProducto.alt = data.nombre;
        divProducto.appendChild(imgProducto);
 
        // Create paragraph element for the product price
        const pPrecio = document.createElement('p');
        pPrecio.textContent = `Precio: $${data.Precio.toString()}`;
        divProducto.appendChild(pPrecio);
 
        // Append the product div to the column div
        divColumn.appendChild(divProducto);
 
        // Append the column div to the container
        contenedorProductos.appendChild(divColumn);
      });
    });
  }